BK

Currently the txtsplit.py doesn't work very well with dashes (-) and I haven't
updated the regex in the python file to correct the issue. Might be easier to
just rename those locations with dashes to underscores instead.
